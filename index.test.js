const test = require('tape')
const index = require('./index')
const item = require('./tests/itemTests')
const charitableFoundation = require('./tests/charityFoundationTests')
const itemCategory = require('./tests/itemCategoryTests')
const category = require('./tests/itemCategoryTests')

test('Criar item', (t) => {
    t.assert(item.create, 'Criou um ítem corretamente')
    t.end()
})

test('Criar categoria', (t) => {
    t.assert(category.create, 'Criou uma categoria corretamente')
    t.end()
})

test('Criar registro de item x categoria', (t) => {
    t.assert(itemCategory.create, 'Criou uma categoria corretamente')
    t.end()
})

test('Criar instituição', (t) => {
    t.assert(charitableFoundation.create, 'Criou uma instituição corretamente')
    t.end()
})

