const { models } = require('../../sequelize');
const { getIdParam } = require('../helpers');

async function getAll(req, res) {
	const categories = await models.category.findAll({order: [['id', 'ASC']]});
	res.header('X-Total-Count', categories.length);
	res.status(200).json(categories);
};

async function getById(req, res) {
	const id = getIdParam(req);
	const category = await models.category.findByPk(id);
	if (category) {
		res.status(200).json(category);
	} else {
		res.status(404).send('404 - Not found');
	}
};

async function create(req, res) {
	if (req.body.id) {
		res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
	} else {
		const model = await models.category.create(req.body);
		res.status(201).json(model);
	}
};

async function update(req, res) {
	const id = getIdParam(req);
		const category = await models.category.update(req.body, {
			where: {
				id: id
			}
		});
		const updatedCategory = await models.category.findByPk(id);
		res.status(200).json(updatedCategory);
};

async function remove(req, res) {
	const id = getIdParam(req);
	await models.item.destroy({
		where: {
			id: id
		}
	});
	res.status(200).end();
};

module.exports = {
	getAll,
	getById,
	create,
	update,
	remove,
};
