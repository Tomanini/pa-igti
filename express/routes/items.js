const { models } = require('../../sequelize');
const { getIdParam } = require('../helpers');

async function getAll(req, res) {
	const items = await models.item.findAll({ include: models.category, order: [['id', 'ASC']]});
	res.header('Access-Control-Expose-Headers', 'X-Total-Count')
	res.header('X-Total-Count', items.length);
	res.status(200).json(items);
};

async function getById(req, res) {
	const id = getIdParam(req);
	const item = await models.item.findByPk(id);
	if (item) {
		res.status(200).json(item);
	} else {
		res.status(404).send(' Not found');
	}
};

async function create(req, res) {
	if (req.body.id) {
		res.status(400).send(`Precisa informar um ID`)
	} else {
		await models.item.create(req.body);
		res.status(201).end();
	}
};

async function update(req, res) {
	const id = getIdParam(req);
		await models.item.update(req.body, {
			where: {
				id: id
			}
		});
		const updatedItem = await models.item.findByPk(id)
		res.status(200).json(updatedItem);
};

async function remove(req, res) {
	const id = getIdParam(req);
	await models.item.destroy({
		where: {
			id: id
		}
	});
	res.status(200).end();
};

module.exports = {
	getAll,
	getById,
	create,
	update,
	remove,
};
