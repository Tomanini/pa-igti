const express = require('express');
const bodyParser = require('body-parser');
const { models, model } = require('../sequelize');
const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt-nodejs')
// var jwt = require('express-jwt');



const routes = {
	users: require('./routes/users'),
	items: require('./routes/items'),
	categories: require('./routes/categories'),
	charitableFoundations: require('./routes/charitableFoundations'),
};

const app = express();
var cors = require('cors')
app.use(cors())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
	res.header('Access-Control-Expose-Headers', 'X-Total-Count')
    next();
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// We create a wrapper to workaround async errors not being transmitted correctly.
function makeHandlerAwareOfAsyncErrors(handler) {
	return async function(req, res, next) {
		try {
			await handler(req, res);
		} catch (error) {
			next(error);
		}
	};
}


//authentication
app.post('/login', (req, res, next) => {
	//esse teste abaixo deve ser feito no seu banco de dados
	async function matchPassword() {
		const user = await models.user.findOne({ where: { login: req.body.login } });
		if(user){
			const validPassword = bcrypt.compareSync(req.body.password, user.password);
			const id = user.id
			if(validPassword){
				const token = jwt.sign({ id }, "secret", {
				    expiresIn: 300 // expires in 5min
				  });
				  return res.json({ auth: true, token: token });
				
			// res.status(200).json({user})
			}else{
				res.status(201).send("Senha incorreta")
			}
			// res.status(200).send(pass)

		}else{
			res.status(201).send('User não encontrado')
		}
		
	}

	matchPassword()

    // if(req.body.user === 'luiz' && req.body.password === '123'){
	//   //auth ok
	  
    //   const id = 1; //esse id viria do banco de dados
    //   const token = jwt.sign({ id }, "secret", {
    //     expiresIn: 300 // expires in 5min
    //   });
    //   return res.json({ auth: true, token: token });
    // }
    
    // res.status(500).json({message: 'Login inválido!'});
})


// app.get('/protected',
//   jwt({ secret: 'shhhhhhared-secret', algorithms: ['RS256'] }),
//   function(req, res) {
//     if (!req.user.admin) return res.sendStatus(401);
//     res.sendStatus(200);
//   });


// We provide a root route just as an example
app.get('/', (req, res) => {
	res.send('API do PA IGTI funcionando. Teste algumas rotas: /api/items, /api/categories');
});

app.get('/api/search', async (req, res) => {
	const search = req.query.search;

	if(search){
		const foundations = await models.charitableFoundation.findAll({
			// attributes: ['id']	,
			include: {
				model: models.item,
				as: 'items',
				where: {
					name: { [Op.iLike]: `%${search}%`} 
					// where: {name: { [Op.iLike]: `%${search}%`} }
				},
				
			},
	
		})

		const foundationsFull = await models.charitableFoundation.findWh
			
		if (foundations) {
			res.status(200).json(foundations);
		} else {
			res.status(200).json();
		}
	}else{
		res.status(200).json()
		// res.end('Não tem o parametro /search')
	}

})

for (const [routeName, routeController] of Object.entries(routes)) {
	if (routeController.getAll) {
		app.get(
			`/api/${routeName}`,
			makeHandlerAwareOfAsyncErrors(routeController.getAll)
		);
	}
	if (routeController.getById) {
		app.get(
			`/api/${routeName}/:id`,
			makeHandlerAwareOfAsyncErrors(routeController.getById)
		);
	}
	if (routeController.create) {
		app.post(
			`/api/${routeName}`,
			makeHandlerAwareOfAsyncErrors(routeController.create)
		);
	}
	if (routeController.update) {
		app.put(
			`/api/${routeName}/:id`,
			makeHandlerAwareOfAsyncErrors(routeController.update)
		);
	}
	if (routeController.remove) {
		app.delete(
			`/api/${routeName}/:id`,
			makeHandlerAwareOfAsyncErrors(routeController.remove)
		);
	}
}

module.exports = app;
