const sequelize = require('../sequelize');
const { pickRandom, randomDate } = require('./helpers/random');

async function reset() {
	

	await sequelize.sync({ force: true });

	await sequelize.models.category.bulkCreate([
		{ name: 'Comida' },
		{ name: 'Roupas' },
		{ name: 'Móveis' },
	]);

	await sequelize.models.item.bulkCreate([
		{ name: 'Arroz', categoryId: 1 },
		{ name: 'Massa' },
		{ name: 'Cobertor' },
	]);

	await sequelize.models.charitableFoundation.bulkCreate([
		{ name: 'Entidade 1', 
			CNPJ: 055676676000116,
			phone: '51 991727738',
			email: 'email@entidade1.com.br',
			address_street: "Rua da entidade 1",
			address_number: "322",
			address_complementary: "Bloco tal",
			address_zipcode: "90650090",
			latitude: 55.51231312,
			longitude: 51.4234242,
			items: [1, 2]
	},
	]);


}

reset();
