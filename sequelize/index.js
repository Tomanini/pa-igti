const { Sequelize } = require('sequelize');
const { applyExtraSetup } = require('./extra-setup');

const sequelize = new Sequelize('dbrjs5rn7t3clv', 'zhmfvsykucueys', '6777abef9774f5a4c3bc615051afaeb2fb14165810ae42aa1ecaa9cb8e88a226', {
  host: 'ec2-34-197-141-7.compute-1.amazonaws.com',
  dialect: 'postgres',
  port: "5432",
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false 
    }
  }
})

const modelDefiners = [
	require('./models/user.model'),
	require('./models/item.model'),
  require('./models/category.model'),
  require('./models/charitablefoundation.model'),
  require('./models/user.model'),
];

// We define all models according to their files.
for (const modelDefiner of modelDefiners) {
	modelDefiner(sequelize);
}

// We execute any extra setup after the models are defined, such as adding associations.
applyExtraSetup(sequelize);

// We export the sequelize connection instance to be used around our app.
module.exports = sequelize;
