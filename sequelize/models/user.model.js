const { DataTypes } = require('sequelize');
const bcrypt = require('bcrypt-nodejs')
// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	
	const User = sequelize.define('user', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING,
			unique: true,
			validate: {
				// We require usernames to have length of at least 3, and
				// only use letters, numbers and underscores.
				is: /^\w{3,}$/
			}
		},
		login: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true
		},
		password: {
			allowNull: false,
			type: DataTypes.STRING,
			unique: false
		},
	}, {
		hooks:{
			beforeCreate: (user) => {
				const salt = bcrypt.genSaltSync();
				user.password = bcrypt.hashSync(user.password, salt);
			  }
		},
		// freezeTableName: true,
		instanceMethods: {
			validPassword: function(password) {
			  return bcrypt.compareSync(password, this.password);
			}
		  }    
	});


	User.prototype.validPassword = async function(password) {
		return await bcrypt.compare(password, this.password);
	  };
};
