const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('charitableFoundation', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING,
		},
		CNPJ: {
			allowNull: true,
			type: DataTypes.STRING
		},
		phone: {
			allowNull: true,
			type: DataTypes.STRING
		},
		email: {
			allowNull: true,
			type: DataTypes.STRING
		},
		address_street: {
			allowNull: true,
			type: DataTypes.STRING
		},
		address_number: {
			allowNull: true,
			type: DataTypes.INTEGER
		},
		address_complementary: {
			allowNull: true,
			type: DataTypes.STRING
		},
		address_zipcode: {
			allowNull: true,
			type: DataTypes.INTEGER
		},
		latitude: {
			allowNull: true,
			type: DataTypes.FLOAT
		},
		longitude: {
			allowNull: true,
			type: DataTypes.FLOAT
		}
	});
};



