const { models } = require(".");

function applyExtraSetup(sequelize) {
	const { item, category, charitableFoundation, user } = sequelize.models;

	category.hasMany(item);
	item.belongsTo(category);
	
	charitableFoundation.belongsToMany(item, { through: 'charitableFoundationItem'})

}

module.exports = { applyExtraSetup };
